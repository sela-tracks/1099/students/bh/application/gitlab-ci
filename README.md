# gitlab-ci

Demonstrate gitlab-ci

## Architecture 

python flask hello world application running on gcp k8s

sources pushed to gitlab,

build an imag using gitlab ci-cd pipeline and pushed to dockerhub

ackage the Helm chart and push it to the GitLab repository during the deploy stage. 

set the ArgoCD application to use this Helm chart from the GitLab repository

## helm chart structure
```

helm-chart/
  ├── Chart.yaml
  ├── values.yaml
  └── templates/
      ├── deployment.yaml
      └── service.yaml

```

## gitlab ci-cd variables 

### Set these 2 variables
Setting is done in GitLab CI/CD (Settings > CI / CD > Variables):

CI_REGISTRY_USER: GitLab username (bhalamish10)
CI_REGISTRY_PASSWORD: GitLab token password (Gitlab -> profile -> token -> gitlab-ci-cd)


### NO NEED TO SET - Automatically provided by GitLab
CI_REGISTRY: The GitLab Container Registry URL.
CI_REGISTRY_IMAGE: The image URL for the current project.


## docker image 

### GitLab Container Registry
Navigate to the Container Registry:
Go to your GitLab project.
In the left sidebar, click on Deploy > Container Registry.

docker pull registry.gitlab.com/sela-tracks/1099/students/bh/application/gitlab-ci:main
